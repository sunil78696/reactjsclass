import React from 'react';
import { Layout, Menu } from 'antd'
import { useHistory } from 'react-router-dom';
import Menus from "../../Constants/menus";

import { LaptopOutlined } from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;

const Sidebar = () => {

    const history = useHistory()

    return (
        <Sider
            style={siderStyle.sider}
            breakpoint="md"
            width="250"
            collapsedWidth="75"
            theme='dark'
            >

            <Menu
                className="mt-3"
                theme="dark"
                selectedKeys={['Dashboard']}
                selectable={true}
                mode="inline"
                >
                {Menus.map((menuItem) =>
                    <>
                        {(menuItem.subMenu && !menuItem.subMenu.length) ?
                            <Menu.Item
                                key={menuItem.name}
                                icon={menuItem.icon}
                                style={siderStyle.menuItem}
                                onClick={() => {
                                    history.push(menuItem.link)
                                }}
                            >
                                {menuItem.name}
                            </Menu.Item> :
                            <Menu.Item
                                key={menuItem.name}
                                icon={menuItem.icon}
                                style={siderStyle.menuItem}
                        >
                            <SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
                                <Menu.Item key="5">option5</Menu.Item>
                                <Menu.Item key="6">option6</Menu.Item>
                                <Menu.Item key="7">option7</Menu.Item>
                                <Menu.Item key="8">option8</Menu.Item>
                            </SubMenu>
                        </Menu.Item>
                        }
                    </>
                )}
            </Menu>
        </Sider>
    )
}

const siderStyle = {
    sider: {
        height: '100vh',
        left: 0,
        position: 'fixed',
    },

    menuItem: {
        listStyleType: 'none',
        padding: '2px',
        marginTop: '10px',
        borderLeft: '2px solid #000042'
    }
}

export default Sidebar;