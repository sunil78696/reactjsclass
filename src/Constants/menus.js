import React from 'react'
import { DeploymentUnitOutlined, UserOutlined } from '@ant-design/icons';

const Menus = [
    {
        name: `Dashboard`,
        link: '/dashboard',
        icon: <DeploymentUnitOutlined />,
        badge: 0,
        subMenu: [],
    },
    {
        name: `Users`,
        link: '/users',
        icon: <UserOutlined />,
        badge: 1,
        subMenu: [{
            name: `Users`,
            link: '/users',
            icon: <UserOutlined />,
            badge: 1,
        }],
    }
]

export default Menus;