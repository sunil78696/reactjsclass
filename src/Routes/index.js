import React from 'react'

import { HashRouter as Router, Route, Switch, Redirect } from "react-router-dom"
import { Login, Dashboard } from "../Screens"


const Routes = () => {
    return(
        <Router>
            <Switch>
                <Route exact={true} path="/" component = {Login} />
                <Route exact={true} path="/login" component = {Login} />
                <Route exact={true} path="/dashboard/" component = {Dashboard} />
                <Route exact={true} path="/dashboard/:data" component = {Dashboard} />
                <Route path="*" >
                    <Redirect path="/" />
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes