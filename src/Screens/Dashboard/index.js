import { Table, Button } from 'antd'
import React, {useState, useEffect} from 'react'

import { NavLink } from 'react-router-dom'
import Sidebar from '../../Components/sidebar'

import { getAllCats } from "../../Services/cats"

import Column from 'antd/lib/table/Column';

import { fileUrl } from "../../Constants/const"

const Dashboard = (props) => {

    const [employees, setEmployees] = useState([])

    useEffect(() => {
        console.log(props.match.params.data)
        // catsList()
    }, [])

    const catsList = async () => {
        let response = await getAllCats()
        if(response.status){
            setEmployees(response.data)
        }
    }

    return (
        <>
            <h1>I am a dashboard page</h1>
            <NavLink to="/login">
                <label>Go to login</label>
            </NavLink>

            <Sidebar />
            <Table
                dataSource={employees}>
                <Column
                    title={"Category Icon"}
                    render={(record) => <img 
                        style={{maxHeight:50}}
                        className="img-fluid img-responsive" 
                        src={`${fileUrl}${record.cat_icon}`} /> 
                    }
                    key={"employee_name"}
                />
                <Column
                    title={"Category Name"}
                    render={(record) => `${record.cat_name}`}
                    key={"id"}
                />
                
                <Column
                    title={"Category Type"}
                    dataIndex={"cat_type"}
                    key={"employee_name"}
                />
            </Table>
        </>
    )
}



export default Dashboard