import http from './axios';

const getAllCats = async () => {
    return new Promise(async (resolve, reject) => {
        let result = await http.post('gameplaycats/get_all_cats', {});
        resolve(result.data);
    })
}

// const getBlog = async (_id) => {
//     return new Promise(async (resolve, reject) => {
//         let result = await http.post('blogs/get', {_id});
//         resolve(result.data);
//     })
// }

// const addBlog = async (addObj) => {
//     return new Promise(async (resolve, reject) => {
//         let token = `Bearer ${localStorage.getItem("admin_token")}`
//         let result = await http.post('blogs/add', addObj, { headers: { Authorization: token } });
//         resolve(result.data);
//     })
// }

// const deleteBlog = async (_id) => {
//     return new Promise(async (resolve, reject) => {
//         let token = `Bearer ${localStorage.getItem('admin_token')}`;
//         let result = await http.post('blogs/delete', {_id}, { headers: { Authorization: token } });
//         resolve(result.data);
//     })
// }

// const updateBlog = async (editObj) => {
//     return new Promise(async (resolve, reject) => {
//         let token = `Bearer ${localStorage.getItem("admin_token")}`;
//         let result = await http.post('blogs/update', editObj, { headers: { Authorization: token } });
//         resolve(result.data);
//     })
// }

// const addBlogImage = async (fileObj) => {
//     const formData = new FormData();
//     return new Promise(async (resolve, reject) => {
//         formData.append("blog_image", fileObj);
//         let token = `Bearer ${localStorage.getItem("admin_token")}`
//         let headers = { 
//             Authorization: token,
//             'Content-Type': 'multipart/form-data'
//         }
//         let result = await http.post('blogs/upload_photo', formData, {headers});
//         resolve(result.data);
//     })
// }

export { getAllCats }